var readline = require('readline');

const inputFile = process.argv[2];
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const lines = [];
rl.on('line', line => {
    line = line.trim();
    if (line) {
        lines.push(parseInt(line));
    }
})

rl.on('close', () => {

    let freq = 0;
    let seen = [freq];
    let searching = true;
    while(searching) {
        // go true frequencies until already seen frequency occurs
        for (const line of lines) {
            freq += line;
            if (seen.indexOf(freq) >= 0) {
                // result has already been seen
                console.log(freq);
                searching = false;
                break;
            }

            seen.push(freq);
        }
    }
})