import java.io.File

if (args.size != 1) {
    throw IllegalArgumentException("Usage: <input file path>")
}

val lines = File(args[0]).readLines(Charsets.UTF_8)

// part 1
val t = lines.asSequence().map { line -> line.toIntOrNull() }.filterNotNull()
val targetFrequence = t.sum()
println("Target Frequence: $targetFrequence")

// part 2
var freq = 0
val seen = mutableSetOf(freq)

repeat@ while(true) {
    for (n in t) {
        freq += n
        if (seen.contains(freq)) {
            println("First repeated frequence: $freq")
            break@repeat
        }
        seen.add(freq)
    }
}
