var readline = require('readline');

const inputFile = process.argv[2];
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

let freq = 0;
rl.on('line', line => {
    line = line.trim();
    if (line) {
        freq += parseInt(line);
    }

})

rl.on('close', () => {
    console.log(freq);
})