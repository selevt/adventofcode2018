import java.io.File
import java.util.regex.Pattern

if (args.size != 1) {
    throw IllegalArgumentException("Usage: <input file path>")
}
/**/
val lines = File(args[0]).readLines(Charsets.UTF_8)

// #1 @ 1,3: 4x4
val pattern = Pattern.compile("""#(?<id>\d+) @ (?<left>\d+),(?<top>\d+): (?<width>\d+)x(?<height>\d+)""");

val pos = mutableSetOf<Pair<Int, Int>>()
val used = mutableSetOf<Pair<Int, Int>>()


lines.forEach { line ->
    toSquares(line).forEach { square ->
        if (!pos.contains(square)) {
            pos.add(square)
        } else {
            used.add(square)
        }
    }
}

println("Duplicates: ${used.size}")

// part 2
// could be optimized to do in one iteration instead
lines.forEach { line ->
    if (!toSquares(line).any { used.contains(it) }) {
        println("No overlap in line ${line}")
    }
}


fun toSquares(input: String): Sequence<Pair<Int, Int>> {
    val matcher = pattern.matcher(input);
    if (!matcher.matches()) {
        return sequenceOf()
    }

    val left = matcher.group("left").toInt();
    val top = matcher.group("top").toInt();
    val width = matcher.group("width").toInt();
    val height = matcher.group("height").toInt();


    return (0 until width).asSequence().map {i ->
        (0 until height).asSequence().map { j ->
            Pair(left + i, top + j)
        }
    }.flatMap { it }
}
