import java.io.File

if (args.size != 1) {
    throw IllegalArgumentException("Usage: <input file path>")
}
/**/
val lines = File(args[0]).readLines(Charsets.UTF_8)

// part 1
var doubles = 0
var triples = 0
lines.forEach { line ->
    // could be optimized to only check targeted values
    val amounts = line.toCharArray().groupBy { it }.map { it.value.size }.toSet()
    if (amounts.contains(2)) { doubles++ }
    if (amounts.contains(3)) { triples++ }
}
println("Doubles: $doubles")
println("Triples: $triples")
println("Checksum ${doubles * triples}")

// part 2
// iterate over all combinations
outer@ for ((idx1, l1) in lines.dropLast(1).withIndex()) {
    for (l2 in lines.drop(idx1 + 1)) {
        val chars1 = l1.toCharArray()
        val chars2 = l2.toCharArray()
        if (chars1.size != chars2.size) { continue }

        var diffChars = 0
        for (i in 0 until chars1.size) {
            if (chars1[i] != chars2[i]) {
                diffChars++
                if (diffChars >= 2) {
                    // seen enough - only looking for diff of 1
                    continue
                }
            }
        }
        if (diffChars == 1) {
            println("Found boxes")
            println("Box 1: $l1")
            println("Box 2: $l2")

            val commonChars = l1.withIndex()
                    .filter { it.value == l2[it.index] }
                    .map { it.value }
                    .joinToString(separator = "")
            println("Commn: $commonChars")
            break@outer
        }
    }
}
